package com.ml4ai.m2s.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import com.ml4ai.rabbitmq.RabbitMQ

@Service
class RabbitMQService {

  @Value("${message.queue.host}")
  var host: String = ""

  @Value("${message.queue.port}")
  var port: Integer = 0

  @Value("${message.queue.username}")
  var username: String = ""

  @Value("${message.queue.password}")
  var password: String = ""

  @Value("${message.queue.virtualHost}")
  var vHost: String = ""

  def build(): RabbitMQ = {
    val rabbitMQAgent = RabbitMQ.builder.host(host).password(password).user(username).port(port).vHost(vHost).build()
    rabbitMQAgent
  }

}

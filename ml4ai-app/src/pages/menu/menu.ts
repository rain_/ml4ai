import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskListPage } from '../task-list/task-list';
import { InfoListPage } from '../info-list/info-list';
import { PublishListPage } from '../publish-list/publish-list';
import { MeListPage } from '../me-list/me-list';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  /**定义Tab页面 */
  public task: any = TaskListPage;
  public info: any = InfoListPage;
  public publish: any = PublishListPage;
  public me: any = MeListPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

}

import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, AlertController } from 'ionic-angular';
import { NetProvider } from '../../providers/net/net';
import { GlobalsProvider } from '../../providers/globals/globals';
import { LoginPage } from '../login/login';
import { IntraCityDistributionPage } from '../intra-city-distribution/intra-city-distribution';
import { EnumPage } from '../enum/enum';

/**
 * Generated class for the PublishListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-publish-list',
  templateUrl: 'publish-list.html',
})
export class PublishListPage {

  @Input() title: string = "发布信息";

  list: any[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private net: NetProvider,
    private globals: GlobalsProvider,
    private popOver: PopoverController,
    private alertController: AlertController) {
  }

  ionViewDidEnter() {
    let me = this;
    this.net.postJson(this.globals.urls.myTask, {}, (data) => {
      if (data && data.code) {
        if (data.code == "200") {
          me.list = data["data"].list;
        } else if (data.code == "401") {
          me.globals.navControl.setRoot(LoginPage);
        }
      }
    }, (xhr) => {

    });
  }

  click(item) {
    let me = this;
    if (item.businessType == 'IntraCityDistribution') {
      this.net.postJson(this.globals.urls.getIntraCityDistribution, { id: item.id }, (response) => {
        if (response && response.code) {
          if (response.code == "200") {
            me.globals.navControl.push(IntraCityDistributionPage, { intraCityDistributionTask: response["data"], parent: me });
          } else if (response.code == "401") {
            me.globals.navControl.setRoot(LoginPage);
          }
        }
      }, (xhr) => { });

    }
  }

  addNew() {
    let me = this;
    let mine = me;
    let p = this.popOver.create(EnumPage, {
      root: "BusinessType",
      callback: (businessType) => {
        switch (businessType) {
          case "IntraCityDistribution":
            me.globals.navControl.push(IntraCityDistributionPage, { intraCityDistributionTask: undefined, parent: mine });
            break;
          default:
            let alert = me.alertController.create({
              title: "提示",
              message: "暂时不支持该类型",
              buttons: ["确定"]
            });
            alert.present();
            break;
        }
      }
    });
    p.present();
  }
}

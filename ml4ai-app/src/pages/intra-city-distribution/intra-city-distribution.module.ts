import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntraCityDistributionPage } from './intra-city-distribution';

@NgModule({
  declarations: [
    IntraCityDistributionPage,
  ],
  imports: [
    IonicPageModule.forChild(IntraCityDistributionPage),
  ],
})
export class IntraCityDistributionPageModule {}

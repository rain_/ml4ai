import { Component } from '@angular/core';

/**
 * Generated class for the TaskListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'task-list',
  templateUrl: 'task-list.html'
})
export class TaskListComponent {

  text: string;

  constructor() {
    console.log('Hello TaskListComponent Component');
    this.text = 'Hello World';
  }

}

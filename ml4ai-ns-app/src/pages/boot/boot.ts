import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { WelcomePage } from '../welcome/welcome';

/**
 * Generated class for the BootPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-boot',
  templateUrl: 'boot.html',
})
export class BootPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public config: ConfigProvider) {
    config.root = this;
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.config.root.navCtrl.setRoot(WelcomePage);
    }, 1000);
  }

}

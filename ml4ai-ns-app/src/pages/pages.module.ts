import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { IndexPage } from './index';
import { ComponentsModule } from '../components/components.module';
import { BootPage } from './boot/boot';
import { WelcomePage } from './welcome/welcome';
@NgModule({
	declarations: [
		IndexPage,
		BootPage,
		WelcomePage
	],
	imports: [
		BrowserModule,
		IonicPageModule,
		ComponentsModule
	],
	entryComponents: [
		IndexPage,
		BootPage,
		WelcomePage
	],
	exports: [
		IndexPage,
		BootPage,
		WelcomePage
	]
})
export class PagesModule { }

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { App } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ComponentsModule } from '../components/components.module';
import { PagesModule } from '../pages/pages.module';
import { RmiProvider } from '../providers/rmi/rmi';
import { StompService } from 'ng2-stomp-service';
import { ConfigProvider } from '../providers/config/config';
import { CommandProvider } from '../providers/command/command';

@NgModule({
  declarations: [
    App,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(App),
    ComponentsModule,
    PagesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    App,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RmiProvider,
    StompService,
    ConfigProvider,
    CommandProvider
  ]
})
export class AppModule {}

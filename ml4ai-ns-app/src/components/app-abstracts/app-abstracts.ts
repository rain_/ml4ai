import { Component, OnInit, OnDestroy } from '@angular/core';
import { RmiProvider, SubscribeHandler } from '../../providers/rmi/rmi';
import { UUID } from 'angular2-uuid';
import { AlertController } from 'ionic-angular';
import { CommandProvider } from '../../providers/command/command';

/**
 * Generated class for the AppAbstractsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-abstracts',
  templateUrl: 'app-abstracts.html'
})
export class AppAbstractsComponent implements OnInit, OnDestroy {

  abstracts: any[] = [];
  load: any = undefined;

  constructor(public command: CommandProvider, private alert: AlertController) {
  }

  init() {
    this.abstracts = [];
    let me = this;
    me.load = (start: number, size: number, parameter: any) => {
      me.command.execute(`loadAbstracts`, {
        parameter: parameter ? parameter : {},
        start: start,
        size: size
      }, (data) => {
        let body = data.body;
        let response = JSON.parse(body);
        if (response.code == '200') {
          me.abstracts = response['data'];
        } else {
          me.alert.create({
            title: '错误'
          });
        }
      }, 10000);
    }
  }

  ngOnInit() {
    this.init();
    this.load(0, 10);
  }

  ngOnDestroy() {

  }
}

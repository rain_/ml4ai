package com.ml4ai.test;

public class ThreadTest {

    private Object lock = new Object();

    public static void main(String... args) throws Exception {
        new ThreadTest().test(args);
    }

    public void test(String... args) throws Exception {

        Thread.currentThread().interrupt();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("发生中断");
        }
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                try {
                    synchronized (lock) {
                        System.out.println("我来了");
                        Thread.sleep(1000);
                    }
                } catch (Exception e) {
                    System.out.println("异常");
                }
            }).start();
        }

    }

}

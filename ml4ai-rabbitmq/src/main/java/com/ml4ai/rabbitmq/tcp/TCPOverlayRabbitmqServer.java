package com.ml4ai.rabbitmq.tcp;

import com.ml4ai.common.Toolkit;
import com.ml4ai.rabbitmq.RabbitMQ;
import lombok.*;
import scala.Tuple2;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TCPOverlayRabbitmqServer {

    public static void createService(RabbitMQ rabbitMQ, int proxyPort, String host, int port) {
        String queueRequest = String.format("%s:%d.request", host, port);
        String queueResponse = String.format("%s:%d.response", host, port);
        rabbitMQ.declareQueue(queueResponse, true, false, false, null);
        rabbitMQ.declareQueue(queueRequest, true, false, false, null);

    }

    @Setter
    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class Session {

        private String id;
        private Long currentReceiveSequence = 0L;
        private Long currentSendSequence = 0L;
        private Socket socket;
        private InputStream inputStream;
        private OutputStream outputStream;

        public void close() {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (Exception e) {

                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {

                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {

                }
            }
        }
    }

    public static class Listener extends Thread implements EventFireAble {

        private String request;
        private String response;
        private BlockingQueue<Tuple2<String, Object[]>> loop;
        private Map<String, Session> sessionMap = new LinkedHashMap<>();
        private boolean live = true;
        private int port;
        private ServerSocketLoop serverSocketLoop;

        @Override
        public BlockingQueue<Tuple2<String, Object[]>> getEventLoop() {
            return loop;
        }

        public Listener(String request, String response, int port) {
            this.request = request;
            this.response = response;
            this.port = port;
            this.loop = new LinkedBlockingQueue<>();
            this.serverSocketLoop = new ServerSocketLoop(this.loop, this.port);
        }

        @Override
        public void run() {
            this.serverSocketLoop.start();
            while (live) {
                try {
                    Tuple2<String, Object[]> e = loop.take();
                    String event = e._1;
                    Object[] parameter = e._2;
                    Session session;
                    switch (event) {
                        case "session.create":
                            session = (Session) parameter[0];
                            sessionMap.put(session.getId(), session);
                            break;
                    }
                } catch (InterruptedException e) {

                }
            }
        }

        public void stopImmediate() {
            live = false;
        }
    }

    public static class UpstreamWorker extends Thread implements EventFireAble {

        private Session session;
        private InputStream inputStream;
        private BlockingQueue<Tuple2<String, Object[]>> loop;
        private boolean live = true;

        public UpstreamWorker(BlockingQueue<Tuple2<String, Object[]>> loop, Session session) {
            this.loop = loop;
            this.session = session;
        }

        @Override
        public BlockingQueue<Tuple2<String, Object[]>> getEventLoop() {
            return loop;
        }

        @Override
        public void run() {
            while (live) {
                try {
                    int read;
                    byte[] buff = new byte[1024 * 1024];
                    while ((read = this.inputStream.read(buff)) != -1) {
                        fire("upstream", session, read, buff);
                    }
                } catch (Exception e) {
                    fire("exception.process.upstream", e);
                }
            }
        }
    }

    public static class ServerSocketLoop extends Thread implements EventFireAble {
        private int port;
        private boolean live = true;
        private ServerSocket serverSocket;
        private BlockingQueue<Tuple2<String, Object[]>> loop;

        @Override
        public BlockingQueue<Tuple2<String, Object[]>> getEventLoop() {
            return loop;
        }

        @Override
        public int fireRetryCount() {
            return 100;
        }

        public ServerSocketLoop(BlockingQueue<Tuple2<String, Object[]>> loop, int port) {
            this.port = port;
            this.loop = loop;
        }

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(port);
            } catch (Exception e) {
                fire("error.init", e);
            }
            while (live) {
                try {
                    Socket client = serverSocket.accept();
                    InputStream inputStream = client.getInputStream();
                    OutputStream outputStream = client.getOutputStream();
                    Session session = Session
                            .builder().socket(client).inputStream(inputStream).outputStream(outputStream)
                            .id(Toolkit.StringHelper.uuid()).currentReceiveSequence(0L)
                            .currentSendSequence(0L)
                            .build();
                    fire("session.create", session);
                } catch (Exception e) {

                }
            }
        }
    }

    public static interface EventFireAble {

        BlockingQueue<Tuple2<String, Object[]>> getEventLoop();

        default int fireRetryCount() {
            return 5;
        }

        default boolean fire(String event, Object... parameter) {
            int limit = fireRetryCount();
            int tryCount = 0;
            BlockingQueue<Tuple2<String, Object[]>> loop = getEventLoop();
            while (tryCount++ < limit) {
                try {
                    loop.put(new Tuple2<>(event, parameter));
                    return true;
                } catch (Exception e) {
                }
            }
            return false;
        }

    }
}

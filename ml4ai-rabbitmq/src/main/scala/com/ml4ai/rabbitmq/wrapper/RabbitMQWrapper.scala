package com.ml4ai.rabbitmq.wrapper

import java.lang
import java.util.function

import com.ml4ai.rabbitmq.RabbitMQ

class RabbitMQWrapper(rabbitMQ: RabbitMQ) {

  def consumeTextScala(queue: String, count: Int, fun: String => Boolean) = {
    rabbitMQ.consumeText(queue, count, new function.Function[java.lang.String, java.lang.Boolean] {
      override def apply(t: String): lang.Boolean = fun(t)
    })
  }

  def consumeScala(queue: String, count: Int, fun: Array[Byte] => Boolean) = {
    rabbitMQ.consume(queue, count, new function.Function[Array[Byte], java.lang.Boolean]() {
      override def apply(t: Array[Byte]): java.lang.Boolean = fun(t)
    })
  }

  def attachInterimConsume(count: Int, fun: Array[Byte] => Boolean) = {
    rabbitMQ.attachInterimConsume(count, new function.Function[Array[Byte], java.lang.Boolean] {
      override def apply(t: Array[Byte]): lang.Boolean = fun(t)
    })
  }

  def attachInterimConsumeText(count: Int, fun: String => Boolean) = {
    rabbitMQ.attachInterimConsumeText(count, new function.Function[java.lang.String, java.lang.Boolean] {
      override def apply(t: java.lang.String): java.lang.Boolean = fun(t)
    })
  }

}

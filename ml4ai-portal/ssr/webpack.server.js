const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const copyWebpackPlugin = require('copy-webpack-plugin')
const { webpackMode } = require('./env/webpack.config')

module.exports = {
    mode: webpackMode,
    target: 'node',
    entry: {
        app: [path.join(__dirname, './server/server-entry.js')],
    },
    output: {
        filename: '../server-entry.js',
        path: path.join(__dirname, './dist/public'),
        publicPath: "/public/",
        libraryTarget: 'commonjs2'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: [{
                    loader: "babel-loader",
                    options: {
                        "presets": [
                            "env",
                            "es2015",
                            "react",
                            "stage-0",
                            "stage-2"
                        ],
                        plugins: [
                            'transform-decorators-legacy',
                            'transform-runtime',
                            'babel-plugin-transform-runtime'
                        ]
                    }
                }],
                exclude: [
                    path.join(__dirname, "../node_modules")
                ],
            }, {
                test: /\.(s)?css$/,
                exclude: /node_modules/,
                use: [
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|jpg|gif|woff|svg|eot|woff2|tff)$/,
                use: 'url-loader?limit=8129',
                //注意后面那个limit的参数，当你图片大小小于这个限制的时候，会自动启用base64编码图片
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(path.join(__dirname, '/dist/*.*'), {
            root: __dirname,
            verbose: true,
            dry: false
        }),
        new copyWebpackPlugin([{
            from: path.resolve(__dirname, 'public', 'assets'),
            to: path.resolve(__dirname, 'dist', 'assets')
        }]),
        new copyWebpackPlugin([{
            from: path.resolve(__dirname, 'public/favicon.ico'),
            to: path.resolve(__dirname, 'dist', 'favicon.ico')
        }]),
        new copyWebpackPlugin([{
            from: path.resolve(__dirname, 'public/sitemap.xml'),
            to: path.resolve(__dirname, 'dist', 'sitemap.xml')
        }]),
        new copyWebpackPlugin([{
            from: path.resolve(__dirname, 'public/robots.txt'),
            to: path.resolve(__dirname, 'dist', 'robots.txt')
        }])
    ]
}
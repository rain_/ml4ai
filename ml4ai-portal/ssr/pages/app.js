import React from 'react'
import Index from '../pages/index'
import { debug } from '../env/config'
import { routes } from '../ctl/routes'
import { Switch, Route, Redirect, MemoryRouter } from 'react-router'
import './css/app.css'

export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Switch>
                    {routes.map(route => <Route path={route.path} component={route.component} />)}
                    <Route component={routes.default.component} />
                </Switch>
            </div>)
    }

}
package com.ml4ai.server.services;

import com.ml4ai.server.domain.Task;
import com.ml4ai.server.domain.base.BusinessType;
import com.ml4ai.server.dto.TaskDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by uesr on 2018/9/9.
 */
public interface TaskService extends BaseService<Task, TaskDTO> {

    String genCode(BusinessType businessType);

}

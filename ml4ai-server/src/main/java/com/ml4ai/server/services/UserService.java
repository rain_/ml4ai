package com.ml4ai.server.services;

import com.ml4ai.server.domain.User;
import com.ml4ai.server.dto.MenuDTO;
import com.ml4ai.server.dto.UserDTO;
import com.ml4ai.server.services.base.BaseService;

import java.util.List;

/**
 * Created by uesr on 2018/9/12.
 */
public interface UserService extends BaseService<User, UserDTO> {

    UserDTO findByLogin(String username);

    List<MenuDTO> findByUserId(Long userId);
}

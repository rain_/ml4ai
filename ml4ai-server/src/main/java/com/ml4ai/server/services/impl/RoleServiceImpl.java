package com.ml4ai.server.services.impl;

import com.ml4ai.server.domain.Role;
import com.ml4ai.server.dto.RoleDTO;
import com.ml4ai.server.repository.RoleRepositroy;
import com.ml4ai.server.repository.UserRepo;
import com.ml4ai.server.services.RoleService;
import com.ml4ai.server.services.base.impl.BaseServiceImpl;
import com.ml4ai.server.services.mappers.RoleMapper;
import com.ml4ai.server.utils.SpringUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by leecheng on 2018/9/24.
 */
@Service
@Transactional
public class RoleServiceImpl extends BaseServiceImpl<Role, RoleDTO> implements RoleService {

    @Override
    public Function<Role, RoleDTO> getConvertEntity2DTOFunction() {
        return SpringUtils.getService(RoleMapper.class)::e2d;
    }

    @Override
    public Function<RoleDTO, Role> getConvertDTO2EntityFunction() {
        return SpringUtils.getService(RoleMapper.class)::d2e;
    }

    @Override
    public JpaRepository<Role, Long> getRepository() {
        return SpringUtils.getService(RoleRepositroy.class);
    }

    @Override
    public List<RoleDTO> findByUserId(Long userId) {
        UserRepo userRepo = SpringUtils.getService(UserRepo.class);
        List<Role> roles = userRepo.getOne(userId).getRoles();
        return roles.stream().map(getConvertEntity2DTOFunction()).collect(Collectors.toList());
    }
}

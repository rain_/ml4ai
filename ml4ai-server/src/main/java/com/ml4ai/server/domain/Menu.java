package com.ml4ai.server.domain;

import com.ml4ai.server.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/2.
 */

@Entity
@Table(name = "T_MENU")
@Getter
@Setter
public class Menu extends BaseAuditEntity {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "c_parent_id")
    private Menu parent;

    @Column(name = "c_menu_name")
    private String menuName;

    @Lob
    @Column(name = "c_menu_data")
    private String menuData;

    @Column(name = "c_details")
    private String details;

    @Column(name="c_type")
    private String type;

    @Column(name = "c_sort_no")
    private Integer sortNo;

}

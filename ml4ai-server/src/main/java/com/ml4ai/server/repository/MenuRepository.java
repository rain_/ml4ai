package com.ml4ai.server.repository;

import com.ml4ai.server.domain.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/2.
 */
public interface MenuRepository extends JpaRepository<Menu, Long> {
}

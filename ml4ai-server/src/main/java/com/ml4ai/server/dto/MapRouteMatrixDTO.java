package com.ml4ai.server.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by leecheng on 2017/11/30.
 */
@Data
public class MapRouteMatrixDTO {

    private String operator;
    private List<MapAddressDTO> origins;
    private List<MapAddressDTO> destinations;

    /**
     * 算路偏好，该参数只对驾车算路(driving)生效。 该服务为满足性能需求，不含道路阻断信息干预。
     * 可选值：
     * 10： 不走高速；
     * <p>
     * 11：常规路线，即多数人常走的一条路线，不受路况影响；
     * <p>
     * 12： 距离较短（考虑路况）：即距离相对较短的一条路线，但并不一定是一条优质路线。计算耗时时，考虑路况对耗时的影响；
     * <p>
     * 13： 距离较短（不考虑路况）：路线同以上，但计算耗时时，不考虑路况对耗时的影响，可理解为在路况完全通畅时预计耗时。
     * 注：除13外，其他偏好的耗时计算都考虑实时路况
     */
    private Integer tactics;

    public final static String OperatorDriving = "driving";
    public final static String OperatorRiding = "riding";
    public final static String OperatorWalking = "walking";

}

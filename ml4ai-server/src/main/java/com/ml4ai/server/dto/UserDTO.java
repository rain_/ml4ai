package com.ml4ai.server.dto;

import com.ml4ai.server.dto.base.BaseAuditDTO;
import lombok.Data;

/**
 * Created by uesr on 2018/9/9.
 */
@Data
public class UserDTO extends BaseAuditDTO {

    private String login;

    private String password;

    private String telephone;

    private String nick;

    private String mail;

    public String getPassword() {
        return password;
    }

    public String getNick() {
        return this.nick;
    }
}

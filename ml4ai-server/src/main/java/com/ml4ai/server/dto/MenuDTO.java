package com.ml4ai.server.dto;

import com.ml4ai.server.dto.base.BaseAuditDTO;
import com.ml4ai.server.utils.annotation.QueryColumn;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by uesr on 2018/9/2.
 */
@Getter
@Setter
@ToString
public class MenuDTO extends BaseAuditDTO {

    @QueryColumn(propName = "parent.id")
    private Long parentId;

    private MenuDTO parent;

    @QueryColumn
    private String menuName;

    @QueryColumn
    private String menuData;

    @QueryColumn
    private String details;

    @QueryColumn
    private String type;

    @QueryColumn
    private Integer sortNo;

    @QueryColumn(queryOper = "isNull", propName = "parent")
    private Boolean parentIsNull;

    private List<MenuDTO> children;
}

package com.ml4ai.server.dto.base;

import com.ml4ai.server.utils.annotation.QueryColumn;
import lombok.*;

import java.time.Clock;

/**
 * Created by uesr on 2018/9/2.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BaseAuditDTO extends BaseDTO {

    private Long createTime = Clock.systemUTC().millis();
    private Long lastUpdateTime;
    private Long createBy;
    private Long lastUpdateBy;
    @QueryColumn
    private String status;

}
